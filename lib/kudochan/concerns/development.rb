require 'slatan'

module Kudochan
  module Concerns
    class Development
      def initialize
      end

      def hear(msg)
        log = getLog
        Slatan::Mouth::Chat.post_message(msg[:channel], log)
      end

      private
      def getLog(offset=0, size=10, level='D')
        logs = []
        File.open(Slatan::Spirit.log_file_path, 'r') do |file|
          file.each_line do |f|
            logs << f if f =~ /^#{level},.*$/
            break if logs.length == size
          end
        end

        logs.reduce(">>>\n") do |content, line|
          "#{content}\n- #{line}"
        end
      end
    end
  end
end
