require 'slatan'

module Kudochan
  module Concerns
    class Integration
      def initialize
        @helps = {
          '通話用の部屋をたてる' => 'へやたてて (オプション: 部屋名)',
        }

        @likely_msgs = [
          "何ですか〜？",
          "今日も頑張りましょう！",
          "人生色々ですよ",
          "ときには休憩も必要ですよ〜",
          "つらい時はゲームしましょう！",
          "最近身体を動かしていますか？一緒に運動しましょう！",
          "美味しいものが食べたいです〜"
        ]
      end

      def hear(event)
        @my_id = Slatan::Affiliation::Self.get[:id]
        @my_name = Slatan::Affiliation::Self.get[:name]

        if event[:text] !~ /^<@#{@my_id}>:?[[:space:]]*(.*)$/ 
          return
        end
        msg = $1.strip
        case msg
        when /^へるぷ|ヘルプ|help$/
          Slatan::Mouth::Ext::Chat.reply(event[:user], event[:channel], build_help('私の使い方ですよ〜', @helps))
        when /^(へや|部屋)[[:space:]]*(たてて|建てて|立てて)[[:space:]]*(.*)$/
          room_name = $3.strip != "" ? $3.strip : SecureRandom.uuid
          url = URI.escape "https://appear.in/#{room_name}"
          Slatan::Mouth::Chat.post_message(event[:channel], "たてましたよ〜\n>>>\n#{url}")
        else
          Slatan::Mouth::Ext::Chat.reply(event[:user], event[:channel], @likely_msgs.sample)
        end
      end

      private
      def build_help(first_msg, helps)
        helps.reduce("#{first_msg}\n>>>") do |msg, (key, command)|
          "#{msg}\n*#{key}*\n    @#{@my_name} #{command}"
        end
      end
    end
  end
end
