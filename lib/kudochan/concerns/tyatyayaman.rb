require 'slatan'

module Kudochan
  module Concerns
    class Tyatyayaman
      def initialize
      end

      def hear(event)
        @my_id = Slatan::Affiliation::Self.get[:id]
        if event[:text] !~ /^<@#{@my_id}>:?[[:space:]]*(.*)$/ 
          return
        end
        event = $1.strip
        case event
        when /^おこして|起して$/
          sysytem('curl https://tyatyayaman.herokuapp.com')
          Slatan::Mouth::Ext::Chat.reply('tyatyayaman', event[:channel], '起きて〜')
        end
      end
    end
  end
end
