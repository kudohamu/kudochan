require "kudochan/version"
require "kudochan/concerns"

module Kudochan
  include Concerns

  class << self
    def init
      @development = Kudochan::Concerns::Development.new
      @tyatyayaman = Concerns::Tyatyayaman.new

      Slatan::Ear.register(@development) do |event|
        my_id = Slatan::Affiliation::Self.get[:id]
        is_message?(event) && event[:text] =~ /^<@#{my_id}>:?[[:space:]]*(ログ|ろぐ|log).*$/
      end

      Slatan::Ear.register(@tyatyayaman) do |event|
        my_id = Slatan::Affiliation::Self.get[:id]
        is_message?(event) && event[:text] =~ /^<@#{my_id}>:?[[:space:]]*(tyatyayaman|ちゃちゃやまん).*$/
      end

      Slatan::Ear.register(Kudochan::Concerns::Integration.new) do |event|
        is_message? event
      end
    end

    def is_message?(event)
      event[:type] == 'message'
    end
  end
end
